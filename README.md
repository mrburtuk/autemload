Google Cloud Storage is used for a Robust storage method, running from a single Multi-Storage bucket in the US Region. 
Using CloudFlare to mask and improve the delivery of the service while protecting it using its reverse proxy method. 

HTTPS rewrites are enabled at both a server and CloudFlare, plus HTTP only is disabled. 